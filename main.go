package main

import (
	caddycmd "github.com/caddyserver/caddy/v2/cmd"
	// plug in Caddy modules here
	_ "github.com/caddyserver/caddy/v2/modules/standard"
	_ "gitlab.com/e2pi/mu/infra/terralite/caddy/module"
)

func main() {
	caddycmd.Main()
}
