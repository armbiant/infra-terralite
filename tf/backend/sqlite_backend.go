package backend

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"os"

	dbx "github.com/go-ozzo/ozzo-dbx"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/e2pi/mu/infra/terralite/tf/spec"
	"go.uber.org/zap"
)

var (
	_ spec.StateBackend = (*sqliteBackend)(nil)
)

func NewSqliteBackend( namspace string, dataSource string,logger *zap.Logger) (spec.StateBackend, error) {

	backend := &sqliteBackend{
		db:         nil,
		namespace:  namspace,
		dataSource: dataSource,
		logger:     logger,
	}

	return backend, nil
}

type sqliteBackend struct {
	namespace  string
	dataSource string
	logger     *zap.Logger
	db         *dbx.DB
}


func (s *sqliteBackend) Logger() *zap.Logger {
	return s.logger
}

func getDBFilePath(source string) (string, error) {
	source = os.ExpandEnv(source)
	return source, nil
}

func (s *sqliteBackend) loadDB() (*dbx.DB, error) {

	if s.db != nil {
		return s.db, nil
	}

	db_file, err := getDBFilePath(s.dataSource)
	if err != nil {
		return nil, err
	}

	db, err := dbx.Open("sqlite3", db_file)
	if err != nil {
		return nil, err
	}

	_, err = db.NewQuery(`
	PRAGMA busy_timeout       = 10000;
	PRAGMA journal_mode       = WAL;
	PRAGMA journal_size_limit = 100000000;
	PRAGMA synchronous        = NORMAL;
	PRAGMA foreign_keys       = TRUE;
   `).Execute()

	if err != nil {
		defer db.Close()
		return nil, fmt.Errorf("failed to setup pragma parameters: [ %s ]", err.Error())
	}

	_, err = db.NewQuery(`
	CREATE TABLE IF NOT EXISTS terraform_states (
		name  TEXT NOT NULL,
		data  BLOB,
		lock  BLOB,
		PRIMARY KEY (name)
	);
	`).Execute( )

	if err != nil {
		defer db.Close()
		return nil, fmt.Errorf("failed to setup database: [ %s ]", err.Error())
	}

	s.db = db
	return db, nil
}

func (s *sqliteBackend) NewState( name string, state []byte, ctx context.Context) (spec.State, error) {
	
	log := s.Logger( )
	log.Info("backend: create new state")
	// sanity check 
	if len(name) == 0 {
		err := errors.New("state name can't be empty string")
		log.Error("failed to create state", zap.Error(err))
		panic(err)
	}
	
	newState := &sqliteState{
		Name_:  name,
		Lock_:  nil,
		Data_: state,
	}

	log.Info("backend: new state created")
	return newState,nil 
	
}


func (s *sqliteBackend) QueryState(name string, ctx context.Context) (spec.State, error) {

	log := s.Logger()
	log.Info("query state", zap.String("state", name))
	
	//db 
	db, err := s.loadDB()
	if err != nil {
		log.Error(
			"query state failed", 
			zap.String("state",name),
			zap.Error(err),
		)
		return nil, err
	}


	//run query
	state := new(sqliteState)
	err = db.NewQuery(`
	SELECT [[name]] as [[name_]], [[data]] as [[data_]], [[lock]] as [[lock_]]
	FROM {{terraform_states}}
	WHERE
	name = {:name};
   `).Bind(dbx.Params{"name": name}).One(&state)

    // state not found?
	if err == sql.ErrNoRows {
		log.Info(
			"query state returned empty", 
			zap.String("state", name),
			zap.Error(err),
		)
		return nil, nil  
	}

	// internal error?
	if err != nil {		
		err := fmt.Errorf("unable to find state [%s]: %v", name, err)
		log.Error(
			"query state failed",
			zap.String("state", name),
			zap.Error(err),
		)
		return nil,err 
	}

	// done
	log.Info(
		"query state completed",
		 zap.String("result","found 1 match"), 
		 zap.String("state", name),
	)
	return state, nil
}

func (s *sqliteBackend) WriteState(state spec.State, ctx context.Context) error {

	log := s.Logger()
	log.Info("writing state", zap.String("name", state.Name()))

	// load database
	db, err := s.loadDB()
	if err != nil {
		return err
	}

	// insert new state or update existing state
	q := `
	  REPLACE INTO terraform_states(name,data,lock)
	  VALUES({:name},{:data},{:lock});
	  `
	_state := (state).(*sqliteState)
	_, err = db.NewQuery(q).Bind(dbx.Params{
		"name": _state.Name_,
		"data": _state.Data_,
		"lock": _state.Lock_,
	}).Execute()

	if err != nil {
		log.Error(
			"failed to write state", 
			zap.String("state",state.Name()),
			zap.String("reason", err.Error()),
		)
		return err
	}

	return nil

}

func (s *sqliteBackend) DeleteState(state spec.State, ctx context.Context) error {

	log := s.Logger()
	log.Info("deleting state", zap.String("state", state.Name()))

	_state, ok := state.(*sqliteState)
	if !ok || _state == nil {
		err := fmt.Errorf("invalid state interface type: [%v]",state)
		log.Error(
			"failed to delete state", 
			zap.String("state", state.Name()),
			zap.Error(err))
		panic(err)
	}

	db, err := s.loadDB()
	if err != nil {
		log.Error(
			"failed to delete state",
			zap.String("state", state.Name()),
			zap.Error(err),
		)
		return  err
	}


	_,err = db.NewQuery(`
	DELETE FROM terraform_states
	WHERE
	name = {:name};
   `).Bind(dbx.Params{"name": state.Name()}).Execute()
	
	if err != nil {		
		
		err := fmt.Errorf("unable to delete state [%s]: %v", state.Name(),err)
		log.Error(
			"failed to delete state",
			zap.String("state", state.Name()),
			zap.Error(err),
		)
		return err 
	}

	log.Info("state deleted", zap.String("state", state.Name()))
	return nil 
}

func (s *sqliteBackend) LockState(state spec.State, lock []byte, ctx context.Context) error {

	log := s.Logger()

	if state == nil {
		err := fmt.Errorf("state interface can not be nil")
		log.Error("failed to acquire lock", zap.String("reason", err.Error()))
		panic(err) 
	}

	if len(lock) == 0 {
		err := fmt.Errorf("lock information can not be empty")
		log.Error("failed to acquire lock", zap.String("reason",err.Error()))
		panic(err) 
	}


	_state, ok := (state).(*sqliteState)
	if !ok {
		err := fmt.Errorf("only sqliteState based implementation of spec.State is supported")
		log.Error("failed to acquire log", zap.String("reason",err.Error()))
		panic(err) 
	}

	// already locked?
	if _state.IsLocked() {
		err := spec.ErrStateLocked
		log.Error(
			"failed to acquire lock",
			zap.String("state", state.Name()),
			zap.String("reason", err.Error()),
	    )
		return err 
	}

	//lock state
	_state.Lock_ = lock 
	if err := s.WriteState(state,ctx); err != nil {
		_state.Lock_ = nil 
		log.Error(
			"failed to acquire lock",
			zap.String("state", state.Name()),
			zap.String("reason", err.Error()),
	    )
		return err 
	}

	check, _ := s.QueryState(_state.Name(),ctx)
	_check   := (check).(*sqliteState)
	log.Error("check lock", zap.Any("curr",string(_state.Lock_)), zap.Any("new", string(_check.Lock_)))

	return nil  
}

func (s *sqliteBackend) UnlockState(state spec.State, ctx context.Context) error {

	log := s.Logger()

	if state == nil {
		err := fmt.Errorf("state interface can not be nil")
		log.Error("failed to free lock", zap.String("reason", err.Error()))
		panic(err)		
	}
	_state, ok := state.(*sqliteState)
	if !ok {
		err := fmt.Errorf("backend.sqliteState is the only supported implementation of spec.State")
		log.Error("failed to free lock", zap.String("reason", err.Error()))
		panic(err)
	}

	
	log.Info("unlocking state", zap.String("state", state.Name()))
	if !_state.IsLocked() {

		err  := fmt.Errorf("state has empty lock: [%v]", _state.Lock_)
		log.Error(
			"failed to free lock",
			zap.String("state", _state.Name_),
			zap.String("reason",err.Error()),
		)
		return err 
	}

	_state.Lock_ = nil 
	err := s.WriteState(_state, ctx)
	if err != nil {
		log.Error(
			"failed to unlock state",
			zap.String("state", _state.Name_),
			zap.String("reason", err.Error()),
		)
		return err 
	}

	log.Info("state unlocked", zap.String("state", _state.Name_))
	return nil 
}
