package backend

import (
	"encoding/json"
	"errors"

	"gitlab.com/e2pi/mu/infra/terralite/tf/spec"
)

var (
	_ spec.State = (*sqliteState)(nil)
)

type sqliteState struct {
	Name_ string
	Lock_ []byte
	Data_ []byte
}



func (s *sqliteState) Name() string {
	return s.Name_
}

func (s *sqliteState) Data() []byte {
	return s.Data_
}

func (s *sqliteState) Lock() []byte {
	return s.Lock_
}

func (s *sqliteState) IsLocked() bool {
	return (s.Lock_ != nil)
}

func (s *sqliteState) SetData(value []byte) {
	s.Data_ = value
}

func (s *sqliteState) GetLockID() (string, error) {

	// state has to be locked
	if !s.IsLocked() {
		return "", errors.New("state is not locked")
	}

	// lock ID
	var m map[string]string 
	if err := json.Unmarshal(s.Lock_,&m); err != nil {
		return "",err 
	}
	id, ok := m["ID"]
	if !ok {
		return "", errors.New("current lock contains no ID(invalid lock)")		
	}
	
	return id,nil 

}