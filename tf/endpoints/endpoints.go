package endpoints

type StateNotFoundResponse struct {
	State string `json:"state"`
 }