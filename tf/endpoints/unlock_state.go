package endpoints

import (
	"context"

	endpoint "github.com/go-kit/kit/endpoint"
	"gitlab.com/e2pi/mu/infra/terralite/tf/spec"
	"go.uber.org/zap"
)


type UnlockRequest struct {
	StateName string  `json:"state_name"`
}

type UnlockedResposne struct { }


func MakeUnlockStateEndpoint( backend spec.StateBackend ) endpoint.Endpoint {

	return func (ctx context.Context, r interface{} ) (interface{},error) {

		log := backend.Logger()
		log.Info("unlock state endpoint")

		request := r.(*UnlockRequest)
	
		//query state
		state, err := backend.QueryState(request.StateName,ctx)
		if err != nil {
			log.Error(
				"unlock state endpoint failed",
				zap.String("state", request.StateName),
				zap.Error(err),
			)
			return nil, err 
		}

		//state does not exist error?
		if state == nil {
			log.Warn(
				"unlock state endpoint failed: state not found", 
				zap.String("state",request.StateName),
			)
			return new(StateNotFoundResponse),nil 
		}
	
		//do unlock  
		err = backend.UnlockState(state, ctx)
		if err != nil {
			log.Error(
				"unlock state endpoint failed",
				zap.String("state", request.StateName),
				zap.Error(err),
			)
			return nil,err
		}

		//done 
		return nil,nil 
	}

} 