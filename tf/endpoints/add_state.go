package endpoints

import (
	"context"

	endpoint "github.com/go-kit/kit/endpoint"
	"gitlab.com/e2pi/mu/infra/terralite/tf/spec"
	"go.uber.org/zap"
)

type AddStateResponse struct {}
type AddStateRequest struct {
	StateName string `json:"state_name"`
	State []byte     `json:"state"`
	LockId string    `json:"lock_id"`
}

func MakeAddStateEndpoint( backend spec.StateBackend ) endpoint.Endpoint {

	return func (ctx context.Context, r interface{} ) (interface{},error) {

		log := backend.Logger()
		log.Info("add state endpoint")

		//request 
		request := r.(*AddStateRequest)

		//query state
		state,err := backend.QueryState(request.StateName,ctx)
		if err != nil {
			log.Error(
				"add state endpoint failed",
				zap.String("state", request.StateName),
				zap.Error(err),
			)
			return nil,err 
		} 
		
		// if state does not exist, create new
		if state == nil {

		   state, err = backend.NewState(request.StateName,request.State, ctx)
		   if err != nil {
			  log.Error(
				"add state endpoint failed",
				 zap.String("state", request.StateName),
				 zap.Error(err),
			  )
			  return nil,err 
		   }

		} 
		
		// check if state is not already locked?
		if state.IsLocked() {
			
			lockID,err := state.GetLockID()
			if err != nil {
				log.Error(
					"add state endpoint failed",
					zap.String("state", request.StateName), 
					zap.Error(err),
				)
				return nil,err 
			}

			if lockID != request.LockId {

				err = spec.ErrStateLocked
				log.Warn(
					"add state endpoint failed",
					zap.String("state", request.StateName),
					zap.Error(err),
				)
				resp := &LockConflictResponse{
					HoldingLock: state.Lock(),
				}
				return resp,err 
			}
		} 

		// save state  
		state.SetData( request.State )
		err = backend.WriteState(state,ctx)
		if err != nil {
			log.Error(
			  "add state endpoint failed",
			   zap.String("state", request.StateName),
			   zap.Error(err),
			)
			return nil,err 
		}

		log.Info("add state endpoint completed")
	    return new(AddStateResponse),nil 
	}

} 