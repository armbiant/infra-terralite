package endpoints

import (
	"context"

	endpoint "github.com/go-kit/kit/endpoint"
	"gitlab.com/e2pi/mu/infra/terralite/tf/spec"
	"go.uber.org/zap"
)

type LockRequest struct {
	StateName string `json:"state_name"`
	StateLock []byte `json:"state_lock"`
}


type LockAcquiredResponse struct{}

type LockConflictResponse struct {
	HoldingLock []byte `json:"holding_lock"`
}

func MakeLockStateEndpoint(backend spec.StateBackend) endpoint.Endpoint {

	return func(ctx context.Context, r interface{}) (interface{}, error) {

		log := backend.Logger()
		log.Info("lock state endpoint")

		//request 
		request := r.(*LockRequest)
		
		// query state
		state, err := backend.QueryState(request.StateName, ctx)
		if err != nil {
			log.Error(
				"lock state endpoint: query state failed",
				zap.String("state", request.StateName),
				zap.Error(err),
			)
			return nil, err
		}
	
		// if state does not exist, create it 
		if state == nil {
			log.Info(
				"lock state endpoint: unable to find state",
				zap.String("state", request.StateName),
				zap.Error(err),
			)
			state,err = backend.NewState(request.StateName,nil,ctx)
			if err != nil {
				log.Error(
					"lock state endpoint: failed to create new state",
					zap.String("state", request.StateName),
					zap.Error(err),
				)
				return nil,err  
			}
		}

		// is state is not new, check if it's already locked
		if state.IsLocked() {
			log.Warn(
				"lock state endpoint: state is already locked, aborting",
				zap.String("state", request.StateName),
			)
			resp := &LockConflictResponse{
				HoldingLock: state.Lock(),
			}
			return resp, nil
		}

		err = backend.LockState(state,request.StateLock, ctx)
		if err != nil {
			log.Error(
				"lock state endpoint: failed to lock state",
				zap.String("state", request.StateName),
				zap.String("reason", err.Error()),
			)
			return nil, err
		}

		log.Info("lock state endpoint: completed")
		return new(LockAcquiredResponse), nil
	}

}
