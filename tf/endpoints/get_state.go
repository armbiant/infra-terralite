package endpoints

import (
	"context"

	endpoint "github.com/go-kit/kit/endpoint"
	"gitlab.com/e2pi/mu/infra/terralite/tf/spec"
	"go.uber.org/zap"
)


type GetStateRequest struct {
	Name string  `json:"name"`
}

type GetStateResponse struct {
	Name      string  `json:"name"`
    State     [ ]byte `json:"state"`
	Lock      [ ]byte `json:"lock"`
}


func MakeGetStateEndpoint( backend spec.StateBackend ) endpoint.Endpoint {

	return func (ctx context.Context, r interface{} ) (interface{},error) {

		log := backend.Logger( )
		log.Info("get state endpoint")

		// request
		request := r.(*GetStateRequest)
	
		// query state
		state, err := backend.QueryState(request.Name, ctx)
		if err != nil {
			
			log.Error(
				"get state endpoint failed",
			 	zap.String("state", request.Name),
				zap.Error(err),
			)
			return nil,err
		}
		
		// state does not exist?
		if state == nil {
			log.Info(
				"get state endpoint aborted: state not found",
				zap.String("state", request.Name),
			)	
			return new(StateNotFoundResponse),nil
		}

		//done 
		log.Info("get state endpoint completed")
		response := &GetStateResponse {
			Name:   state.Name(),
			State:  state.Data(),
			Lock:   state.Lock(),
		}
		return response,nil 
	}

} 