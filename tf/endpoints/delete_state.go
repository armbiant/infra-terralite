package endpoints

import (
	"context"

	endpoint "github.com/go-kit/kit/endpoint"
	"gitlab.com/e2pi/mu/infra/terralite/tf/spec"
	"go.uber.org/zap"
)

type DeleteStateRequest struct {
	StateName string `json:"state_name"`
}
type DeleteStateResponse struct { }

func MakeDeleteStateEndpoint(backend spec.StateBackend) endpoint.Endpoint {

	return func(ctx context.Context, r interface{}) (interface{}, error) {

		log := backend.Logger()
		log.Info("delete state endpoint")

		//request 
		request := r.(*DeleteStateRequest)
		
		// query state
		state, err := backend.QueryState(request.StateName, ctx)
		if err != nil {
			log.Error(
				"delete state endpoint failed",
				zap.String("state", request.StateName),
				zap.Error(err),
			)
			return nil, err
		}

		// state does not exist? 
		if state == nil {
			log.Warn(
				"delete state endpoint: state does not exist",
				zap.String("state", request.StateName),
			)
			response := new(StateNotFoundResponse)
			return response, nil 
		}

		//TODO: shouldn't state be locked prior to deletion?

		//delete state
		err = backend.DeleteState(state,ctx)
		if err != nil {
			log.Error(
				"delete state endpoint: failed to delete state",
				zap.String("state", request.StateName),
				zap.Error(err),
			)
			return nil, err
		}

		log.Info("delete state endpoint completed")
		return new(DeleteStateResponse), nil
	}

}
