package http

import (
	"context"
	"fmt"
	"io"
	"net/http"

	gk "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"gitlab.com/e2pi/mu/infra/terralite/tf/endpoints"
	"gitlab.com/e2pi/mu/infra/terralite/tf/spec"
)

func RegisterHttpLockStateEndpoint( service spec.StateBackend,router *mux.Router) {

	endpoint := gk.NewServer( 
	    endpoints.MakeLockStateEndpoint(service),
		lockStateRequestDecode,
		lockStateResponseEncode)
	router.NewRoute().Path("/{name}").Methods("LOCK").Handler(endpoint)
}


func lockStateRequestDecode( ctx context.Context, r *http.Request ) (interface{},error) {		

	
	var state string 
	{
		qvars := mux.Vars(r)
		s, ok := qvars["name"]
		if !ok {
			return nil, fmt.Errorf(
			  "failed to parse state name from request: %v", 
			  r.URL)
		}
		state = s  
	}

	var lock []byte 
	{
		l, err := io.ReadAll(r.Body)
		if err != nil {
			return nil, err 
		}
		lock = l 
		defer r.Body.Close()
	}

	newRequest := &endpoints.LockRequest{ 
		StateName: state, 
		StateLock: lock,
	}
	
	return newRequest,nil 
}

func lockStateResponseEncode( ctx context.Context, w http.ResponseWriter, r interface{}) error {

	// success?
	if _, ok := r.(*endpoints.LockAcquiredResponse); ok {
		w.WriteHeader(http.StatusOK)
		return nil 
	}

	// state is already locked?
	if resp, ok := r.(*endpoints.LockConflictResponse); ok {
		w.WriteHeader(http.StatusConflict)
		w.Write(resp.HoldingLock)
		return nil
	} 

	// state does not exist?
	if _,ok := r.(*endpoints.StateNotFoundResponse); ok {
		w.WriteHeader(http.StatusNotFound)
		return nil 
	}

	// should not reach here
	return fmt.Errorf("unsupported response type: %v", r)

}


