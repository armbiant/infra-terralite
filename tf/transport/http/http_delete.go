package http

import (
	"context"
	"fmt"
	"net/http"

	gk "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"gitlab.com/e2pi/mu/infra/terralite/tf/endpoints"
	"gitlab.com/e2pi/mu/infra/terralite/tf/spec"
)

func RegisterHttpDeleteStateEndpoint( service spec.StateBackend,router *mux.Router) {

	endpoint := gk.NewServer( 
	    endpoints.MakeDeleteStateEndpoint(service),
		deleteStateRequestDecode,
		deleteStateResponseEncode)
	router.NewRoute().Path("/{name}").Methods(http.MethodDelete).Handler(endpoint)
}


func deleteStateRequestDecode( ctx context.Context, r *http.Request ) (interface{},error) {		

	var stateName string 
	{
		vars := mux.Vars(r)
		s,ok := vars["name"]
		if !ok {
			return nil, fmt.Errorf(
				"failed to parse state name from URL: %v",
				 r.URL)
		}
		stateName = s
	}
	 
	request := &endpoints.DeleteStateRequest {
		StateName: stateName,
	}

	 return request, nil 
}

func deleteStateResponseEncode( ctx context.Context, w http.ResponseWriter, r interface{}) error {

	if _, ok := r.(*endpoints.StateNotFoundResponse); ok {
		w.WriteHeader(http.StatusNotFound)
	} else if _,ok := r.(*endpoints.DeleteStateResponse); ok {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
	}
	
	return nil 
}


