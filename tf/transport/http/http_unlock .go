package http

import (
	"context"
	"fmt"
	"net/http"

	gk "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"gitlab.com/e2pi/mu/infra/terralite/tf/endpoints"
	"gitlab.com/e2pi/mu/infra/terralite/tf/spec"
)

func RegisterHttpUnlockStateEndpoint( service spec.StateBackend,router *mux.Router) {

	endpoint := gk.NewServer( 
	    endpoints.MakeUnlockStateEndpoint(service),
		unlockStateRequestDecode,
		unlockStateResponseEncode)
	router.NewRoute().Path("/{name}").Methods("UNLOCK").Handler(endpoint)
}


func unlockStateRequestDecode( ctx context.Context, r *http.Request ) (interface{},error) {		

	var stateName string 
	{
		vars := mux.Vars(r)
		s,ok := vars["name"]
		if !ok {
			return nil, fmt.Errorf(
				"failed to parse state name from URL: %v",
				 r.URL)
		}
		stateName = s
	}
	 
	unlockRequest := &endpoints.UnlockRequest {
		StateName: stateName,
	}

	 return unlockRequest, nil 
}

func unlockStateResponseEncode( ctx context.Context, w http.ResponseWriter, r interface{}) error {


	w.WriteHeader(http.StatusOK)
	return nil 
}


