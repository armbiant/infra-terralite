package http

import (
	"context"
	"fmt"
	"io"
	"net/http"

	gk "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"gitlab.com/e2pi/mu/infra/terralite/tf/endpoints"
	"gitlab.com/e2pi/mu/infra/terralite/tf/spec"
)

func RegisterHttpPostStateEndpoint( service spec.StateBackend,router *mux.Router) {

	endpoint := gk.NewServer( 
	    endpoints.MakeAddStateEndpoint(service),
		newStateRequestDecode,
		newStateResponseEncode)
	router.NewRoute().Path("/{name}").Methods(http.MethodPost).Handler(endpoint)
}


func newStateRequestDecode( ctx context.Context, r *http.Request ) (interface{},error) {		


	var stateName string 
	{
		vars := mux.Vars(r)
		s,ok := vars["name"]
		if !ok {
			return nil, fmt.Errorf(
				"failed to parse state name from URL: %v",
				 r.URL)
		}
		stateName = s
	}
	var state []byte 
	{
		s,err := io.ReadAll(r.Body) 
		defer r.Body.Close()
		if err != nil {
			return nil,err 
		}
		state = s 
	}

	lockID := r.URL.Query().Get("ID")
	newRequest := &endpoints.AddStateRequest{
		StateName: stateName,
		State: state,
		LockId: lockID,
	}

	return newRequest,nil 
}

func newStateResponseEncode( ctx context.Context, w http.ResponseWriter, r interface{}) error {

	// lock conflict?
	if resp,ok := r.(*endpoints.LockConflictResponse); ok {

		w.WriteHeader(http.StatusLocked)
		w.Write(resp.HoldingLock)
		return nil 
	}

	// state not found?
	if _,ok := r.(*endpoints.StateNotFoundResponse); ok {
		w.WriteHeader(http.StatusNotFound)
		return nil 
	}

	//ok
	w.WriteHeader(http.StatusOK)
	return nil 

}


