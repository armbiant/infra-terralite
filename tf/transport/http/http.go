package http

import (
	"github.com/gorilla/mux"
	"gitlab.com/e2pi/mu/infra/terralite/tf/spec"
)

func RegisterEndpoints( service spec.StateBackend, router *mux.Router) {

	RegisterHttpGetStateEndpoint(service,router)
	RegisterHttpPostStateEndpoint(service,router)
	RegisterHttpLockStateEndpoint(service,router)
	RegisterHttpUnlockStateEndpoint(service,router)
	RegisterHttpDeleteStateEndpoint(service,router)

}
