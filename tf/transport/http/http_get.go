package http

import (
	"context"
	"fmt"
	"net/http"

	gk "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"gitlab.com/e2pi/mu/infra/terralite/tf/endpoints"
	"gitlab.com/e2pi/mu/infra/terralite/tf/spec"
)

func RegisterHttpGetStateEndpoint( 
	service spec.StateBackend,
	router *mux.Router) {

	endpoint := gk.NewServer( 
		endpoints.MakeGetStateEndpoint(service),
		getStateRequestDecode,
		getStateResponseEncode)

	router.HandleFunc("/{name}", endpoint.ServeHTTP).Methods(http.MethodGet)

}

func getStateRequestDecode( ctx context.Context, r *http.Request ) (interface{},error) {		
	qvars := mux.Vars(r)
	if name,ok := qvars["name"]; ok {
		return &endpoints.GetStateRequest{
			Name: name,
		},
		nil 
	}

	return nil, fmt.Errorf("URL does not contain state: %v", r.URL) 
}

func getStateResponseEncode( ctx context.Context, w http.ResponseWriter, r interface{}) error {

	    if _, ok := r.(*endpoints.StateNotFoundResponse); ok {

			w.WriteHeader(http.StatusNotFound)
			return nil 
		}

		resp := r.(*endpoints.GetStateResponse)
		state := resp.State
		w.WriteHeader(http.StatusOK)
		w.Write(state)
		
		return nil 	
}


