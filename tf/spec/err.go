package spec


type StateError int
const ( 
	ErrUnknown        = StateError(0)
	ErrStateLocked    = StateError(1)
	ErrStateNotLocked = StateError(2)
	ErrStateExists    = StateError(3)
)



func (s StateError) Error( ) string {
	
	switch s {
	case ErrStateLocked:
		return "state is already locked by another user"
	case ErrStateNotLocked:
		return "state is not locked"
	case ErrStateExists:
		return "state already exists"
	}

	return "unknown error ecountered while processsing state request"
}

