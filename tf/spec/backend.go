package spec

import (
	"context"

	"go.uber.org/zap"
)


type StateBackend interface {

	Logger( ) *zap.Logger

	NewState( name string, state []byte, ctx context.Context) (State,error)
	QueryState( name string, ctx context.Context) (State,error)
	
	DeleteState( state State, ctx context.Context ) error
	WriteState(  state State, ctx context.Context ) error 
	
	LockState( state State, lock []byte, ctx context.Context ) error 
	UnlockState( state State, ctx context.Context ) error 

}

