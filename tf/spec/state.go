package spec

// Terraform state interface
type State interface {

	Name( ) string
	
	//lock 
	IsLocked() bool 
	Lock( ) []byte 
	GetLockID() (string,error) 
	
	//state data
	Data( ) []byte
	SetData( value []byte )  
}

