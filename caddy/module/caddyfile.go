package module

import (
	"github.com/caddyserver/caddy/v2/caddyconfig/caddyfile"
	"github.com/caddyserver/caddy/v2/caddyconfig/httpcaddyfile"
	"github.com/caddyserver/caddy/v2/modules/caddyhttp"
)

var (
	_ caddyfile.Unmarshaler       = (*Terralite)(nil)
)

func init() {
	httpcaddyfile.RegisterHandlerDirective("terralite", parseCaddyfile)
}


func parseCaddyfile(helper httpcaddyfile.Helper) (caddyhttp.MiddlewareHandler, error) {
	t := new(Terralite)
	err := t.UnmarshalCaddyfile(helper.Dispenser)
	return t, err
}

// terralite {
//   state       = "<state_name>"
//	 data_source = "<db_file>"
// }
func (tf *Terralite) UnmarshalCaddyfile(d *caddyfile.Dispenser) error {

	//skip keyword: terralite
	if !d.Next() {
		return d.ArgErr()
	}

	//defult config properties
	tf.State  = "dev"
	tf.DBFile = "dev.db"

	//read config properties
	for d.Next() {
		switch d.Val() {
		case "state":
			{
				if !d.NextArg() {
					return d.ArgErr()
				}
				tf.State = d.Val()
			}
		case "db_file":
			{
				if !d.NextArg() {
					return d.ArgErr()
				}
				tf.DBFile = d.Val()
			}
		}
	}
	
	return nil
}

