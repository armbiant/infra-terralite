package module

import (
	"github.com/caddyserver/caddy/v2"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

var (
	_ caddy.Module      = (*Terralite)(nil)
)

type Terralite struct {
	
	State  string   `json:"state,omitempty"`
	DBFile string   `josn:"db_file,omitempty"`

	ctx        caddy.Context 
	logger     *zap.Logger
	router     *mux.Router
}


func (Terralite) CaddyModule() caddy.ModuleInfo {

	return caddy.ModuleInfo{
		ID: "http.handlers.terralite",
		New: func() caddy.Module {
			return new(Terralite)
		},
	}
}


func init() {
	caddy.RegisterModule(Terralite{})
}
