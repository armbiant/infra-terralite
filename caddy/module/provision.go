package module

import (
	"github.com/caddyserver/caddy/v2"
	"github.com/gorilla/mux"
	"gitlab.com/e2pi/mu/infra/terralite/tf/backend"
	"gitlab.com/e2pi/mu/infra/terralite/tf/transport/http"
)
var (
	_ caddy.Provisioner = (*Terralite)(nil)
)

// Provision sets up the app.
func (tf *Terralite) Provision(ctx caddy.Context) error {

	tf.ctx    = ctx
	tf.logger = ctx.Logger()
	tf.router = mux.NewRouter()
	backend, err := backend.NewSqliteBackend(
		tf.State,
		tf.DBFile,
		tf.logger)

	if err != nil {
		return err 
	}

	// register endpoints 
	http.RegisterEndpoints(backend,tf.router)

	return nil
}