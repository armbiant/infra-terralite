package module

import (
	"net/http"

	"github.com/caddyserver/caddy/v2/modules/caddyhttp"
)


var (
	_ caddyhttp.MiddlewareHandler = (*Terralite)(nil)
)

// ServeHTTP implements caddyhttp.MiddlewareHandler
func (t *Terralite) ServeHTTP(w http.ResponseWriter, r *http.Request, next caddyhttp.Handler) error {

	newRequest := r.WithContext(t.ctx)
	t.router.ServeHTTP(w,newRequest)
	return next.ServeHTTP(w,newRequest)
}


